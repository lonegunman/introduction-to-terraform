# workspace

`terraform workspace` 命令可以用来管理当前使用的工作区。我们在状态管理章节中介绍过[工作区](../../2.Terraform基础概念/2.状态管理.md#状态的隔离存储)的概念。

该命令包含一系列子命令，我们将会一一介绍。

## 用法

`terraform workspace <subcommand> [options] [args]`