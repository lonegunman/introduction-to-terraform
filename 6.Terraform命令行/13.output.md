# output

`terraform output` 命令被用来提取状态文件中输出值的值。

## 用法

`terraform output [options] [NAME]`

如果不添加参数，`output` 命令会展示根模块内定义的所有输出值。如果指定了 `NAME`，只会输出相关输出值。

可以使用以下参数：

* `-json`：设置该参数后 Terraform 会使用 JSON 格式输出。如果指定了 `NAME`，只会输出相关输出值。该参数搭配 `jq` 使用可以构建复杂的流水线
* `-raw`：设置该参数后 Terraform 会将指定的输出值转换为字符串，并将该字符串直接打印到输出中，不带任何特殊格式。这在使用 shell 脚本时很方便，但它仅支持字符串、数字和布尔值。处理复杂的数据类型时还请使用 `-json`。
* `-no-color`：不输出颜色
* `-state=path`：状态文件的路径，默认为 `terraform.tfstate`。启用远程 Backend 时该参数无效

**注意**：设置 `-json` 或 `-raw` 参数时，Terraform 状态中的任何敏感值都将以纯文本显示。有关详细信息，请参阅状态中的[敏感数据](../3.Terraform代码的书写/3.输入变量#在命令行输出中隐藏值-sensitive)。

## 样例

假设有如下输出值代码：

```hcl
output "instance_ips" {
  value = aws_instance.web.*.public_ip
}

output "lb_address" {
  value = aws_alb.web.public_dns
}

output "password" {
  sensitive = true
  value = var.secret_password
}
```

列出所有输出值：

```bash
$ terraform output
instance_ips = [
  "54.43.114.12",
  "52.122.13.4",
  "52.4.116.53"
]
lb_address = "my-app-alb-1657023003.us-east-1.elb.amazonaws.com"
password = <sensitive>
```

注意password输出值定义了sensitive = true，所以它的值在输出时会被隐藏：

```bash
$ terraform output password
password = <sensitive>
```

要查询负载均衡的DNS地址：

```bash
$ terraform output lb_address
my-app-alb-1657023003.us-east-1.elb.amazonaws.com
```

查询所有主机的IP：

```bash
$ terraform output instance_ips
test = [
    54.43.114.12,
    52.122.13.4,
    52.4.116.53
]
```

使用-json和jq查询指定主机的ip：

```bash
$ terraform output -json instance_ips | jq '.value[0]'
```

## 在自动化环境下运行 `terraform output` 命令

`terraform output` 命令默认以便于人类阅读的格式显示，该格式可以随着时间的推移而改变以提高易读性。

对于脚本编写和自动化，请使用 `-json` 生成稳定的 JSON 格式。您可以使用 JSON 命令行解析器（例如 [`jq`](https://stedolan.github.io/jq/)）解析输出：

```shell
$ terraform output -json instance_ips | jq -r '.[0]'
54.43.114.12
```

如果要在 shell 脚本中直接使用字符串值，可以转而使用 `-raw` 参数，它将直接打印字符串，没有额外的转义或空格。

```shell
$ terraform output -raw lb_address
my-app-alb-1657023003.us-east-1.elb.amazonaws.com
```

`-raw` 选项仅适用于 Terraform 可以自动转换为字符串的值。处理复杂类型的值（例如对象）时还请改用 `-json`（可以与 `jq` 结合使用）。

Terraform 字符串是 Unicode 字符序列而不是原始字节，因此 `-raw` 输出在包含非 ASCII 字符时将采用 UTF-8 编码。如果您需要不同的字符编码，请使用单独的命令（例如 `iconv`）对 Terraform 的输出进行转码。