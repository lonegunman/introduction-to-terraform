# destroy

`terraform destroy` 命令可以用来销毁并回收所有由 Terraform 配置所管理的基础设施资源。

虽然我们一般不会删除长期存在于生产环境中的对象，但有时我们会用 Terraform 管理用于开发目的的临时基础设施，在这种情况下，您可以在完成后使用 `terraform destroy` 来方便地清理所有这些临时资源。

## 用法

`terraform destroy [options]`

该命令是以下命令的快捷方式：

```shell
terraform apply -destroy
```

因此，此命令接受 [`terraform apply`](4.apply) 所支持的大部分选项，但是它不支持 `-destroy` 模式搭配指定计划文件的用法。

我们还可以通过运行以下命令创建推测性销毁计划，以查看销毁的效果：

```shell
terraform plan -destroy
```

该命令会以 `destroy` 模式运行 [`terraform plan`](14.plan) 命令，显示准备要销毁的变更，但不予执行。

**注意**：`terraform apply` 的 `-destroy` 选项仅存在于 Terraform v0.15.2 及更高版本中。对于早期版本，必须使用 `terraform destroy` 才能获得 `terraform apply -destroy` 的效果。