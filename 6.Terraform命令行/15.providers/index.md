# providers

`terraform providers` 命令显示有关当前工作目录中代码的 [Provider 声明](../../2.Terraform基础概念/1.Provider#provider-的声明)的信息，以帮助我们了解每个被需要的 Provider 是从何而来。

该命令是含有内嵌子命令，这些子命令我们会逐个解释。

## 用法

```shell
terraform providers
```
