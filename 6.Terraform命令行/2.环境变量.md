# 环境变量

Terraform使用一系列的环境变量来定制化各方面的行为。如果只是想简单使用Terraform，我们并不需要设置这些环境变量；但他们可以在一些不常见的场景下帮助我们改变Terraform的默认行为，或者是出于调试目的修改输出日志的级别。

## `TF_LOG`

该环境变量可以设定 Terraform 内部日志的输出级别，例如：

```bash
$ export TF_LOG=TRACE
```

Terraform 日志级别有 `TRACE`、`DEBUG`、`INFO`、`WARN` 和 `ERROR`。`TRACE` 包含的信息最多也最冗长，如果 `TF_LOG` 被设定为这五级以外的值时 Terraform 会默认使用 `TRACE`。

如果在使用 Terraform 的过程中遇到未知的错误并怀疑是 Terraform 或相关插件的 bug，请设置 `TF_LOG` 级别后收集输出的日志并提交给相关人员。

有志于获取 Terraform 认证的读者请注意，该知识点近乎属于必考。

## `TF_LOG_PATH`

该环境变量可以设定日志文件保存的位置。注意，如果TF_LOG_PATH被设置了，那么 `TF_LOG` 也必须被设置。举例来说，想要始终把日志输出到当前工作目录，我们可以这样：

```bash
$ export TF_LOG_PATH=./terraform.log
```

## `TF_INPUT`

该环境变量设置为 `"false"` 或 `"0"` 时，等同于运行 Terraform 相关命令行命令时添加了参数 `-input=false`。如果你想在自动化环境下避免 Terraform 通过命令行的交互式提示要求给定输入变量的值而是直接报错时(无 `default` 值的输入变量，无法通过任何途径获得值)可以设置该环境变量：

```bash
$ export TF_INPUT=0
```

## `TF_VAR_name`

我们在介绍输入变量赋值时介绍过，可以通过设置名为 `TF_VAR_name` 的环境变量来为名为 `"name"` 的输入变量赋值：

```bash
$ export TF_VAR_region=us-west-1
$ export TF_VAR_ami=ami-049d8641
$ export TF_VAR_alist='[1,2,3]'
$ export TF_VAR_amap='{ foo = "bar", baz = "qux" }'
```

## `TF_CLI_ARGS` 以及 `TF_CLI_ARGS_name`

`TF_CLI_ARGS` 的值指定了附加给命令行的额外参数，这使得在自动化 CI 环境下可以轻松定制 Terraform 的默认行为。

该参数的值会被直接插入在子命令后(例如 plan)以及通过命令行指定的参数之前。这种做法确保了环境变量参数优先于通过命令行传递的参数。

例如，执行这样的命令：`TF_CLI_ARGS="-input=false" terraform apply -force`，它等价于手工执行 `terraform apply -input=false -force`。

`TF_CLI_ARGS` 变量影响所有的 Terraform 命令。如果你只想影响某个特定的子命令，可以使用 `TF_CLI_ARGS_name` 变量。例如：`TF_CLI_ARGS_plan="-refresh=false"`，就只会针对 `plan` 子命令起作用。

该环境变量的值会与通过命令行传入的参数一样被解析，你可以在值里使用单引号和双引号来定义字符串，多个参数之间以空格分隔。

## `TF_DATA_DIR`

`TF_DATA_DIR` 可以修改 Terraform 保存在每个工作目录下的数据的位置。一般来说，Terraform 会把这些数据写入当前工作目录下的 `.terraform` 文件夹内，但这一位置可以通过设置 `TF_DATA_DIR` 来修改。

大部分情况下我们不应该设置该变量，但有时我们不得不这样做，比如默认路径下我们无权写入数据时。

该数据目录被用来保存下一次执行任意命令时需要读取的数据，所以必须被妥善保存，并确保所有的 Terraform 命令都可以一致地读写它，否则 Terraform 会找不到 Provider 插件、模块代码以及其他文件。

## `TF_WORKSPACE`

多环境部署时，可以使用此环境变量而非 `terraform workspace select your_workspace` 来切换 workspace。使用 `TF_WORKSPACE` 允许设置使用的工作区。

比如：

```shell
export TF_WORKSPACE=your_workspace
```

建议仅在非交互式使用中使用此环境变量，因为在本地 shell 环境中，很容易忘记设置了该变量并将变更执行到错误的环境中。

可以在[这里](22.workspace/index)阅读工作区的更多信息。

## `TF_IN_AUTOMATION`

如果该变量被设置为非空值，Terraform 会意识到自己运行在一个自动化环境下，从而调整自己的输出以避免给出关于该执行什么子命令的建议。这可以使得输出更加一致且减少非必要的信息量。

## `TF_REGISTRY_DISCOVERY_RETRY`

该变量定义了尝试从 registry 拉取插件或模块代码遇到错误时的重试次数。

## `TF_REGISTRY_CLIENT_TIMEOUT`

该变量定义了发送到 registry 连接请求的超时时间，默认值为 10 秒。可以这样设置超时：

```bash
$ export TF_REGISTRY_CLIENT_TIMEOUT=15
```

## `TF_CLI_CONFIG_FILE`

该变量设定了 Terraform 命令行配置文件的位置：

```bash
$ export TF_CLI_CONFIG_FILE="$HOME/.terraformrc-custom"
```

## `TF_PLUGIN_CACHE_DIR`

`TF_PLUGIN_CACHE_DIR` 环境变量是[配置插件缓存目录](1.命令行配置文件#provider-插件缓存)的另一种方法。你也可以使用 `TF_PLUGIN_CACHE_MAY_BREAK_DEPENDENCY_LOCK_FILE` 环境变量设置 [`plugin_cache_may_break_dependency_lock_file` 配置项](1.命令行配置文件#允许-provider-缓存跳过依赖锁文件检查)

## `TF_IGNORE`

如果 `TF_IGNORE` 设置为 `"trace"`，Terraform 会在调试信息中输出被忽略的文件和目录。该配置与 `.terraformignore` 文件搭配时对调试大型代码仓库相当有用：

```shell
export TF_IGNORE=trace
```