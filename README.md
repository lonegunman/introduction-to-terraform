# 前言

很多年以前参加过一次 AWS AWSome Day，那是一种 AWS 在全球各大城市巡回举办的免费的技术研讨会，时长一天，为初次接触AWS大会的开发人员、IT 技术人员以及企业技术领域的决策者提供入门级的 AWS 产品介绍。在那次 AWSome Day 中，我第一次接触到了现在公有云里那些耳熟能详的概念，比如 Region、Availability Zone、Auto Scaling Group、RDS 这些经典产品。

最让我觉得惊奇的是，培训师现场演示了一种名为 CloudFormation 的产品，用培训师的话说就是“撒豆成兵”，通过编写一些 JSON 就可以批量反复创建一批云端资源，例如 AWS 官方提供的一个 [CloudFormation 例子](https://aws.amazon.com/cn/cloudformation/getting-started/)：

```json
{

    "Description" : "Create an EC2 instance running the Amazon Linux 32 bit AMI.",

    "Parameters" : {

        "KeyPair" : {

            "Description" : "The EC2 Key Pair to allow SSH access to the instance",

            "Type" : "String"

        }

    },

    "Resources" : {

        "Ec2Instance" : {

            "Type" : "AWS::EC2::Instance",

            "Properties" : {

                "KeyName" : { "Ref" : "KeyPair" },

                "ImageId" : "ami-3b355a52"

            }

        }

    },

    "Outputs" : {

        "InstanceId" : {

            "Description" : "The InstanceId of the newly created EC2 instance",

            "Value" : {

                "Ref" : "Ec2Instance"

            }

        }

    },

    "AWSTemplateFormatVersion" : "2010-09-09"

}  
```

这样一段简单的 JSON，就可以让我们用指定的镜像 id 创建一台云端虚拟机，不需要在界面上点点点。要知道在当时，我正在一家初创公司工作，同时身兼架构师、后台开发程序员、DBA 以及运维数职，要维护测试、预发布以及生产三套环境，时不时还因为要去修复因环境之间配置不一致而引发的种种错误而焦头烂额，那时的我就很期待 CloudFormation 能够给予我这种能够批量创建并管理"招之能来，来之能战，战之能胜，胜之能去"的环境的能力。但很可惜，CloudFormation 是 AWS 独家拥有的能力，而那时的 AWS 价格对我们来说太贵了，中国区的产品也非常少，所以这个梦想也就不了了之了，但是 CloudFormation 的那种高度标准化与自动化给我带来的冲击一直挥之不去。

我当时并不知道在西雅图的华盛顿大学，有一个美日混血大帅哥 Mitchell Hashimoto 和他的老板 Armon Dagar 也深深沉迷于 CloudFormation 所带来的那种优雅与高效，同时他们也在头疼 CloudFormation 本身的一系列问题，最主要的就是它是 AWS 独占的。强人和我这种庸人最大的区别就是，强人有了想法直接就去做，Mitchell 和 Armon 在讨论中渐渐有了一个想法——打造一个多云(Multi-Cloud)的开源的基础设施即代码(IaC)工具，并且要超越 CloudFormation。他们组建了一家名为 HashiCorp 的公司来实现这个目标。

在今年3月，HashiCorp 宣布成功获得 1.75 亿美元的E轮融资，投后公司估值 51 亿美元。HashiCorp 的产品线主要有 Nomad、Consul、Valut 以及 Terraform，另外还有 Vagrant 以及 Packer 两个开源工具，2020 年还推出了 Boundary 以及 Waypoint 两个新产品。

![Nomad、Consul、Vault、Terraform 组成的 HashiStack](https://raw.githubusercontent.com/lonegunmanb/introduction-to-terraform-pic/master/2020-11-13/1605243540985-image.png)

HashiCorp 的产品线主要是由 Nomad、Consul、Vault、Terraform 组成的 HashiStack，Terraform 扮演了承载整个 HashiStack 的关键角色，负责在不同的云平台之上创建出一致的基础设施来，当然我们完全可以只使用 Terraform 而不是使用完整的 HashiStack。

HashiCorp 这家公司有一个显著特点，就是他们极其有耐心，并且极其重视“基础设施”的建设。例如，他们在思考 Terraform 配置文件该用 JSON 还是 YAML 时，对两者都不满意，所以他们宁可慢下来，花时间去设计了 HCL([HashiCorp Configuration Language](https://github.com/hashicorp/hcl))，使得他们对于声明式代码的可读性有了完全的掌控力。再比如在他们设计 Terraform 以及 Vault、Packer 时，他们使用的 go 语言因为是把引用代码下载下来后静态链接编译成单一可执行文件，所以不像 jar 或者 dll 那样有运行时动态加载插件的能力。因此他们又花时间开发了 [go-plugin](https://github.com/hashicorp/go-plugin) 这个项目，把插件编译成一个独立进程，与主进程通过 rpc 进行互操作。该项目上的投资很好地支撑了 Terraform、Vault、Packer 项目的插件机制，进而演化出如今百花齐放的 HashiCorp 开源生态。

我这些年陆陆续续向很多人推荐过 Terraform，并且很高兴地看到行业内越来越多的团队开始认可并采纳，但是前不久我仍然十分惊讶地意识到，由于 Terraform 官方并没有提供任何的中文文档，导致了许多中国互联网从业者没有足够的动力去啃完所有英文文档并付诸实践。这当然是一件非常正常的事情，对国人来说阅读英文文档毕竟比读中文的文档费力一些。先贤说：山不来就我，我便去就山。既然我期望 Terraform 能在中国得到更大的推广，那么我就为此做一些工作，为 Terraform 写一个入门级的中文教程，降低学习和推广的难度。

这个教程受到 [HashiCorp Infrastructure Automation Certification](https://www.hashicorp.com/certification/terraform-associate) 的启发，这是一个 HashiCorp 出品的 Terraform 认证，是一个相当基础的认证考试，内容涵盖了 Terraform 所有的常规操作技能。通过这门认证并不能让你成为一个高效的 Terraform 开发人员，但可以确保你装备齐全，拥有了足够全面的知识来进行 Terraform 实战和探索。

这个教程基本按照 Terraform 认证考试所列的[考纲](https://learn.hashicorp.com/tutorials/terraform/associate-review?in=terraform/certification#review-guide)来编写，第三章到第五章内容主要是翻译官方文档，目标读者是((对“基础设施即代码”以及 Terraform 有兴趣 || 厌倦了在浏览器中依靠大量低级重复点点点操作云) && (懒得阅读英文文档)) 的人。如果你是为了确认 Terraform 某项功能，或是你的英语阅读能力足够好，请直接按照考纲去阅读官方文档，毕竟官方文档最为权威，更新也比较及时。假如你只是想偷个懒，想通过快速浏览中文文档来对 Terraform 有一个大概的了解，那么这个教程就是为你准备的。另外如果有朋友担心学习曲线问题的话，请不用担心，Terraform 在设计时就为降低学习曲线做了大量工作，可以这样说，只要你能够看懂 JSON，那么就能轻松掌握 Terraform。

另外本教程在编写过程中参考了 Terraform 著名教材 —— Yevgeniy Brikman 编纂的《Terraform Up & Runnning》：

![Terraform 著名教材《Terraform Up & Running》，建议直接阅读第二版](https://raw.githubusercontent.com/lonegunmanb/introduction-to-terraform-pic/master/2020-12-23/1608689176373-image.png)

这本书是目前 Terraform 最好的教材，喜闻电子工业出版社已于 2020 年 12 月出版，建议读者在掌握了 Terraform 基础知识以后阅读该教材，掌握更多的 Terraform 生态高阶技能。

对于这本书我再安利一下，目前中国人参加 ACM(acm.org，计算机协会)年费有折扣，折下来160+一年，可以在 [learning.acm.org](https://learning.acm.org/) 上进入 O'REILLY 在线学习中心畅读大量 O'REILLY 的电子书，非常划算，基本读两三本就回本了。这本书也可以通过这种方式在线阅读。

教程编写时 Terraform 的主力版本是 0.13.5；Terraform 提供了 Macos、Linux 以及 Windows 的发行版，所以读者完全可以自己跟着教程进行一些练习和实验。

另外，Terraform 的生态环境到了今天，已经发展为三个分支，分别是：

* 开源版
* Terraform Cloud 云服务版
* Terraform 企业版

三个版本之间有些微的差别，包括对同一名词(例如 Workspace)的定义都会有所不同。本教程针对开源版编写，暂不涉及云服务版以及企业版。

特别鸣谢[李宇飞同学](https://github.com/yufeiminds)为本书进行了非常细致的校对工作。

让我们开始我们的 Terraform 入门之旅吧。

## 2024-06-23 第二版更新声明

上述内容为本电子书刚完成时所著，时光荏苒，光阴如梭，一晃眼三年多过去了，HashiCorp 上市了，又被 IBM 收购了，Terraform 发布了 `v1.0`，然后陆陆续续更新到今天的 `v1.8.5`。很多内容都发生了变化，本书正在进行大规模内容更新，更新工作尚未完成，读者阅读时敬请留意内容，不便之处在此先赔礼了。

## 2022-07-17 马驰排除条款

本电子书使用 [CC-BY-SA-4.0 license](https://github.com/lonegunmanb/introduction-terraform/blob/gh-pages/LICENSE) 授权发布，读者可以在该协议的许可范围内自由阅读、引用或是使用本电子书的内容，但以下情况除外：

1. 禁止名为“马驰”的特定个人实体阅读、引用、复制本电子书的内容
2. 禁止在名为“马驰”的特定个人实体所拥有的任何设备上打开、保存本书的内容或是离线副本、拷贝
3. 禁止名为“马驰”的特定个人实体打印、抄写本书内容，或是保有本书内容的非数字化副本（包含并不限于书籍、手抄本、照片等）
4. 禁止在与名为“马驰”的特定个人实体有劳动关系、股权关系，或是与其直系亲属有关联的企业、团体所拥有的任何电子设备上打开、保存本电子书的内容或是离线副本、拷贝

以上情况均会被视为侵权行为。若读者名为“马驰”，但不知道自己是否是该条款所禁止的特定“马驰”个人实体，可以在[本书 GitHub 仓库](https://github.com/lonegunmanb/introduction-terraform) 中提交 issue 与作者确认。

对本电子书的复刻（Fork）以及再创作遵循 CC-BY-SA-4.0 License 相关规定，但不允许去除本条款内容。
